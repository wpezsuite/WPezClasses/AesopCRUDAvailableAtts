<?php

namespace WPezSuite\WPezClasses\AesopCRUDAvailableAtts;

// No WP? Die! Now!!
if ( ! defined( 'ABSPATH' ) ) {
    header( 'HTTP/1.0 403 Forbidden' );
    die();
}


class ClassAesopCRUDAvailableAtts {

    protected $_arr_atts;
    protected $_arr_types_supported;
    protected $_arr_types_values;
    protected $_str_type;
    protected $_str_default;
    protected $_str_desc;
    protected $_str_tip;
    protected $_arr_values;


    public function __construct() {

        $this->setPropertyDefaults();

    }

    protected function setPropertyDefaults() {

        $this->_arr_atts            = false;
        $this->_arr_types_supported = [

            'color'        => true,
            'media_upload' => true,
            'radio_col'    => true,
            'radio_row'    => true,
            'select'       => true,
            'text'         => true,
            'text_area'    => true,
            'text_small'   => true,
        ];
        $this->_arr_types_values    = [
            'radio_col' => true,
            'radio_row' => true,
            'select'    => true,
        ];
        $this->_str_type            = false;
        $this->_str_default         = '';
        $this->_str_desc            = '';
        $this->_str_tip             = '';
        $this->_arr_values          = [];
        $this->_arr_values_default  = [
            'value' => 'todo',
            'name'  => 'TODO: Values for select not set'
        ];
    }


    public function updateTypesSupported( $arr_types = [] ) {

        if ( is_array( $arr_types ) ) {

            $this->_arr_types_supported = array_merge( $this->_arr_types_supported, $arr_types );

            return true;
        }

        return false;

    }


    public function updateTypesValues( $arr_types = [] ) {

        if ( is_array( $arr_types ) ) {

            $this->_arr_types_values = array_merge( $this->_arr_types_values, $arr_types );

            return true;
        }

        return false;

    }

    /**
     * The array of atts for the current component
     *
     * @param bool $arr_atts
     *
     * @return bool
     */
    public function setAtts( $arr_atts = false ) {

        if ( is_array( $arr_atts ) ) {

            $this->_arr_atts = $arr_atts;

            return true;

        }

        return false;
    }

    public function setType( $str_type = false ) {

        if ( isset( $this->_arr_types_supported[ $str_type ] ) && $this->_arr_types_supported[ $str_type ] === true ) {

            $this->_str_type = $str_type;

            return true;

        }

        return false;
    }

    public function setDefault( $str_default = '' ) {

        return $this->setString( '_str_default', $str_default );

    }

    public function setDesc( $str_desc = '' ) {

        return $this->setString( '_str_desc', $str_desc );

    }

    public function setTip( $str_tip = '' ) {

        return $this->setString( '_str_tip', $str_tip );

    }

    public function pushValue( $str_value = false, $str_name = false, $bool_default = false ) {

        if ( is_string( $str_value ) && is_string( $str_name ) ) {

            $this->_arr_values[] = [
                'value' => $str_value,
                'name'  => $str_name
            ];

            if ( $bool_default === true ) {
                $this->setDefault( $str_value );
            }

            return true;

        }

        return false;
    }

    public function loadValues( $arr_values = [] ) {

        if ( is_array( $arr_values ) ) {

            foreach ( $arr_values as $arr_value ) {

                if ( is_array( $arr_value ) && isset( $arr_value['value'] ) && isset( $arr_value['name'] ) ) {

                    $bool_default = false;
                    if ( isset( $arr_value['default'] ) ) {
                        $bool_default = (boolean)$arr_value['default'];
                    }
                    $this->pushValue( $arr_value['value'], $arr_value['name'], $bool_default );
                }
            }

            return true;
        }

        return false;
    }


    protected function setString( $str_prop, $str ) {

        if ( is_string( $str ) ) {

            $this->$str_prop = $str;

            return true;

        }

        return false;
    }

    public function unsetAtt( $str_key = '' ) {

        unset( $this->_arr_atts[ $str_key ] );

    }

    public function getAtt() {

        if ( is_string( $this->_str_type ) ) {

            $arr_ret = [
                'type'    => $this->_str_type,
                'default' => $this->_str_default,
                'desc'    => $this->_str_desc,
                'tip'     => $this->_str_tip,
            ];

            if ( isset( $this->_arr_types_values[ $this->_str_type ] ) && $this->_arr_types_values[ $this->_str_type ] === true ) {

                if ( empty( $this->_arr_values ) ) {
                    $this->_arr_values[] = $this->_arr_values_default;

                }

                $arr_ret = array_merge( $arr_ret, [ 'values' => $this->_arr_values ] );
                // TODO - test to make sure default is in this array
            }

            return $arr_ret;
        }

        return false;
    }

    public function resetValues() {

        $this->_arr_values = [];
    }


    public function insertAtt( $str_key = false, $int_position = false, $arr_att = false ) {

        if ( ! is_string( $str_key ) ) {
            return false;
        }
        if ( ! is_array( $arr_att ) ) {
            $arr_att = $this->getAtt();
        }
        if ( ! is_array( $this->_arr_atts ) || ! is_array( $arr_att ) ) {
            return false;
        }
        if ( ! is_integer( $int_position ) ) {

            $int_position = count( $this->_arr_atts );
        }

        $arr_inset = [
            $str_key => $arr_att
        ];

        //    var_dump($int_position);

        $arr_first = array_splice( $this->_arr_atts, 0, $int_position );
        //   var_dump( $arr_first );
        $this->_arr_atts = array_merge( $arr_first, $arr_inset, $this->_arr_atts );

        return true;

    }

    public function moveAtt( $str_key = false, $int_position = 0 ) {

        if ( array_key_exists( $str_key, $this->_arr_atts ) && is_array( $this->_arr_atts[ $str_key ] ) ) {

            $arr_temp     = $this->_arr_atts[ $str_key ];
            $int_position = (integer)$int_position;

            $this->insertAtt( $str_key, $int_position, $arr_temp );

            return true;
        }

        return false;
    }

    public function updateAtt( $str_att = false, $arr_update = false_ ) {

        if ( isset( $this->_arr_atts[ $str_att ] ) && is_array( $arr_update ) ) {

            // TODO check type to make sure values in update is also an array
            $this->_arr_atts[ $str_att ] = array_merge( $this->_arr_atts[ $str_att ], $arr_update );

            return true;
        }

        return false;

    }

    public function updateAttType( $str_att = false, $str_new_type = false ) {

        if ( in_array( $str_new_type, $this->_arr_types_supported ) ) {

            return $this->updateAttMaster( $str_att, $str_new_type, 'type' );
        }

        return false;
    }

    public function updateAttDesc( $str_att = false, $str_new_desc = false ) {

        if ( is_string( $str_new_desc)){

            return $this->updateAttMaster( $str_att, $str_new_desc, 'desc' );

        }
        return false;
    }

    public function updateAttTip( $str_att = false, $str_new_tip = false ) {

        if ( is_string( $str_new_tip)){

            return $this->updateAttMaster( $str_att, $str_new_tip, 'tip' );

        }
        return false;
    }

    public function updateAttDefault( $str_att = false, $str_new_default = false ) {

        if ( is_string( $str_new_default)){

            return $this->updateAttMaster( $str_att, $str_new_default, 'default' );

        }
        return false;
    }

    public function updateAttValues( $str_att = false, $str_new_values = false ) {

        if ( is_array( $str_new_values)){

            return $this->updateAttMaster( $str_att, $str_new_values, 'values' );

        }
        return false;
    }


    protected function updateAttMaster( $str_att, $str_new_value, $str_key ) {

        if ( isset( $this->_arr_atts[ $str_att ] ) ) {

            $this->_arr_atts[ $str_att ][ $str_key ] = $str_new_value;

            return true;

        }

        return false;

    }

    public function getAtts() {

        return $this->_arr_atts;
    }


}
