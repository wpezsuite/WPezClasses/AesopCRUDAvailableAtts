## WPezClasses: Aesop CRUD Available Atts

__The ezWay to CRUD the stock Aesop Story Engine component settings (as found in ASE's \admin\includes\available.php)__

The WordPress plugin Aesop Story Engine has a hook (read: filter) aesop_avail_components that allows you manipulate the plugin's components' settings / values. This class makes that customizing ez'ier.     

> --
>
> Special thanks to JetBrains (https://www.jetbrains.com/) and PhpStorm (https://www.jetbrains.com/phpstorm/) for their support of OSS and its devotees. 
>
> --

### Simple Example

Important: If you add new fields to a component you must also use the corresponding shortcode_atts_* (filter) to add those fields/values to the $atts, as those fields are not in the atts defaults (and therefore will be ignored by the shortcode_atts() function).

``` 
add_filter('aesop_avail_components', 'filterMyASE');

function filterMyASE($shortcodes){
    if ( isset( $shortcodes['quote']['atts'] ) && is_array( $shortcodes['quote']['atts'] ) ) {
    
        $new_crud = new ClassAesopCRUDAvailableAtts();
    
        $new_crud->setAtts( $shortcodes['quote']['atts'] );
    
        // remove field: type
        $new_crud->unsetAtt( 'type' );
        // remove field: background
        $new_crud->unsetAtt( 'background' );
        //remove field: text
        $new_crud->unsetAtt( 'text' );
        // remove field: img
        $new_crud->unsetAtt( 'img' );
        
        // add a new field of type: select
        $new_crud->setType( 'select' );
        // set the new field's desc
        $new_crud->setDesc( 'Quote Style' );
        // set the new field's tip 
        $new_crud->setTip('Pick one of the preconfigured styles');
        // clear any values in the values property
        // note: not doing this allows you to carry a given set of value from one field to the next. 
        $new_crud->resetValues();
        // load the values for the select
        $arr_styles = [
            ...TBD...
            ];
        $new_crud->loadValues( $arr_styles);
        // insert the field with a name and a position.  
        $new_crud->insertAtt( 'my_custom_style', 0 );
        
        // move the quote field to position 0 in the array
        $new_crud->moveAtt( 'quote', 0 );
        // update the values for the align field
        $arr_align = [
            ...TBD...
         ];
         $new_crud->updateAttValues( 'align', $arr_align );
         // change the type of align to radio_row
         $new_crud->updateAttType( 'align', 'radio_row' );
         
         // get the array of Atts and update $shortcodes
         $shortcodes['quote']['atts'] = $new_crud->getAtts();
    
    }
    return $shortcodes;
}
```


### FAQ

**1) Can I use this in my plugin or theme?**

Yes, but to be safe, please change the namespace. 

### HELPFUL LINKS

http://aesopstoryengine.com/

http://aesopstoryengine.com/developers/

https://github.com/hyunsupul/aesop-core

### TODO

- Provide a better example(s)



### CHANGE LOG

- V0.0.4 - 16 April 2019
  - Updated: Namespace

- v0.0.3 - 13 Feb 2019
  - Time to share this with the world